import * as express from "express";
import * as bodyParser from "body-parser";

const eventQueue: { task: number }[] = [];

const app = express();

app.use(bodyParser.json());

app.post('/event', (req, res) => {
    let now = new Date();
    console.log(`${now.toISOString()} - Event request from ${req.ip}: ${JSON.stringify(req.body)}`);
    if (typeof (req.body.task) === "number") {
        eventQueue.push(req.body);
        res.send({ success: true });
    } else {
        res.send({ success: false });
    }
});

app.post('/status', (req, res) => {
    let now = new Date();
    console.log(`${now.toISOString()} - Received from ${req.ip}: ${JSON.stringify(req.body)}`);
    res.send({ success: true, ...eventQueue.pop() });
});

app.listen(8000, () => console.log(`App listening on port ${8000}!`))
