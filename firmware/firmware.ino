#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

WiFiMulti wifiMulti;

const char *ssid = "GL-MT300N-V2-09d";
const char *password = "goodlife";

void setup()
{
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();

    Serial.println("Initializing Module");
    for (uint8_t t = 4; t > 0; t--)
    {
        Serial.flush();
        delay(1000);
    }
    Serial.println("Connecting to WiFi");
    wifiMulti.addAP(ssid, password);
}

void updateStatus()
{
    Serial.println("Update status");
    DynamicJsonDocument doc(200);

    HTTPClient http;
    http.begin("http://192.168.8.163:8000/status");
    http.addHeader("content-type", "application/json");
    int httpCode = http.POST("{\"status\":0}");
    if (httpCode > 0)
    {
        Serial.print("Communication successful: ");
        Serial.println(httpCode);

        if (httpCode == HTTP_CODE_OK)
        {
            String payload = http.getString();
            DeserializationError error = deserializeJson(doc, payload);
            if (error)
            {
                Serial.print("deserializeJson() failed:");
                Serial.println(error.c_str());
            }
            else
            {
                // Task successful
                int task = doc["task"];
                if (task > 0)
                {
                    Serial.print("Task received: ");
                    Serial.println(task);
                }
            }
            Serial.println(payload);
        }
    }
    else
    {
        Serial.print("Error: ");
        Serial.println(http.errorToString(httpCode).c_str());
    }
    http.end();
}

void loop()
{
    static unsigned long prev = 0;
    unsigned long now = millis();

    if ((wifiMulti.run() == WL_CONNECTED))
    {
        if (now - prev > 1000)
        {
            updateStatus();
            prev = now;
        }
    }
}
